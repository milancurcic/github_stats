from datetime import datetime, timezone
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import sys

from model import Repository, Issue, Comment, load_json

# Date range
date1 = datetime(2019, 10, 15, tzinfo=timezone.utc)
date2 = datetime(2119, 10, 30, tzinfo=timezone.utc)

if len(sys.argv) != 2:
    raise Exception("Please provide exactly 1 argument")

json_file = sys.argv[1]

print("GitHub repository information:")
print("JSON filename: %s" % json_file)
print()

repo = load_json(json_file, Repository)
print("Statistics")

print()
print("Contributors who created new issues and how many:")
print("%-3s   %-22s %s" % ("  N", "GitHub ID", "Number of issues"))
users = {}
for i in repo.issues:
    comment_date = parse(i.date)
    if not (date1 <= comment_date and comment_date <= date2):
        continue
    user_count = users.get(i.user, 0)
    users[i.user] = user_count + 1
users = sorted(users.items(), key=lambda x: x[1], reverse=True)
for i, (u, n) in enumerate(users):
    print("%3d.  %-22s %4d" % (i+1, u, n))

print()
print("Contributors who commented and how many times:")
print("%-3s   %-22s %s" % ("  N", "GitHub ID", "Number of comments"))
users = {}
for i in repo.issues:
    comment_date = parse(i.date)
    if not (date1 <= comment_date and comment_date <= date2):
        continue
    # Count creating an issue as one comment
    user_count = users.get(i.user, 0)
    users[i.user] = user_count + 1
    # Count all comments posted under an issue
    for c in i.comments:
        comment_date = parse(c.date)
        if not (date1 <= comment_date and comment_date <= date2):
            continue
        user_count = users.get(c.user, 0)
        users[c.user] = user_count + 1
users = sorted(users.items(), key=lambda x: x[1], reverse=True)
for i, (u, n) in enumerate(users):
    print("%3d.  %-22s %4d" % (i+1, u, n))
