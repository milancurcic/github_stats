from dataclasses import dataclass
import sys

from github3 import login

from model import Repository, Issue, Comment, load_toml, save_json

@dataclass
class GitHub:
    user: str
    token: str

@dataclass
class Account:
    github: GitHub

if len(sys.argv) != 2:
    raise Exception("Please provide exactly 1 argument")

github_repository = sys.argv[1]

username, reponame = github_repository.split("/")
json_file = "data-%s-%s.json" % (username, reponame)

print("GitHub repository information:")
print("URL: https://github.com/%s" % github_repository)
print("User: %s" % username)
print("Repo: %s" % reponame)
print("JSON filename: %s" % json_file)
print()

account = load_toml("account.toml", Account)
user = account.github.user
pw = account.github.token

gh = login(user, pw)
repo = gh.repository(username, reponame)
issues = []
for issue in repo.issues(state="all"):
    print(issue.number)
    comments = []
    for comment in issue.comments():
        comments.append(Comment(
            user=str(comment.user.login),
            date=str(comment.created_at),
            text=str(comment.body_text)
            ))
    issues.append(Issue(
            number=issue.number,
            user=str(issue.user.login),
            date=str(issue.created_at),
            title=str(issue.title),
            text=str(issue.body_text),
            comments=comments
            ))
repository = Repository(
        name=str(repo.full_name),
        issues=issues
        )

print("Saving data into '%s'" % json_file)
save_json(json_file, repository)
