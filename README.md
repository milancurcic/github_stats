# GitHub Stats

```
$ conda env create -f environment.yml
$ conda activate gh_stats
$ python download_issues.py j3-fortran/fortran_proposals
$ python stats.py data-j3-fortran-fortran_proposals.json
```

The `download_issues.py` scripts accepts the repository name and downloads all
GitHub issues and saves them into `data-j3-fortran-fortran_proposals.json`. This
script requires you to create a file `account.toml`:
```toml
[github]
user = "your_github_ID"
token = "TOKEN"
```
You can create a GitHub TOKEN at the url: https://github.com/settings/tokens

The script `stats.py` accepts the data json filename, loads it and creates a
statistics. It does not require GitHub, your credentials or internet access, all
the information is present in the local json file.
